import Vapor
import JWT
import OAuthCommon

public struct ScopeValidationMiddleware: Middleware {
    
    private let scope: Scope
    
    public init(scope: Scope) {
        self.scope = scope
    }
    
    public func respond(to request: Request, chainingTo next: Responder) throws -> EventLoopFuture<Response> {
        guard let token = request.http.headers.bearerAuthorization?.token.data(using: .utf8) else { throw Abort(.unauthorized) }
        let authorizationPayload = try JWT<JWTAuthorizationPayload>(unverifiedFrom: token)
        let scopes = authorizationPayload.payload.scopes?.map { Scope(identifier: $0, description: "") } ?? []
        let matchedScopes = scopes.filter { $0.main == scope.main }
        guard matchedScopes.count > 0 else {
            throw Abort(.unauthorized)
        }
        if let sub = scope.sub {
            guard matchedScopes.contains(where: { $0.sub == sub || $0.sub == "*" }) else {
                throw Abort(.unauthorized)
            }
        }
        return try next.respond(to: request)
    }
}
