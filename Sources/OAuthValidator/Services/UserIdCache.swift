import Vapor

public final class UserIdCache: Service {
    
    var userId: UUID?
    
    public init() { }
}

public extension Request {
    
    func requireUserId() throws -> UUID {
        let cache = try privateContainer.make(UserIdCache.self)
        guard let userId = cache.userId else {
            throw Abort(.unauthorized)
        }
        return userId
    }
}

extension Request {
    
    func setUserId(uuid: UUID) throws {
        let cache = try privateContainer.make(UserIdCache.self)
        cache.userId = uuid
    }
}
