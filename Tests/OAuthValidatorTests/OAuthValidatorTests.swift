import XCTest
@testable import OAuthValidator

final class OAuthValidatorTests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(OAuthValidator().text, "Hello, World!")
    }

    static var allTests = [
        ("testExample", testExample),
    ]
}
