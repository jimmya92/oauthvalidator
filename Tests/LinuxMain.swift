import XCTest

import OAuthValidatorTests

var tests = [XCTestCaseEntry]()
tests += OAuthValidatorTests.allTests()
XCTMain(tests)
